import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import { ProgressBar } from 'react-bootstrap';
import Axios from "axios";
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import Button from '@mui/material/Button';
import Tooltip from '@mui/material/Tooltip';

function DepartmentList(){
  const [users, setUsers] = React.useState([]);
    const url="https://reqres.in/api/users"

  React.useEffect(() => {
    Axios.get(url).then((response) => {
      setUsers(response.data.data);
    });
  }, []);

    const updateTableList=
      users.map(function(user){
        return   <tr align = "center">
          <td> {user.first_name} </td>
          <td>
            <p>{user.description}</p>
          </td>
          {/* <td>{user.Action}</td> */}
                         
          <td><div class="btn-group" role="group" aria-label="Basic example">
          <Tooltip title = "Edit">
                           <Button variant="outlined" className = "btn_green" ><EditIcon fontSize="small"/></Button></Tooltip>
                           <Tooltip title = "Delete">
                           <Button variant="outlined"  className = "btn_red"><DeleteIcon fontSize="small"/></Button></Tooltip>
                           {/* <button type="button" class="btn btn-primary" style={{ color: "blue", backgroundColor: "white", bordercolor: "blue", border: "1px solid #0099CC" }}><EditIcon/></button>
                           <button type="button" class="btn btn-primary"  style={{ color: "blue", backgroundColor: "white", bordercolor: "red", border: "1px solid #0099CC" }}><DeleteIcon/></button> */}
                           {/* <EditIcon/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                            <DeleteIcon/> */}
                         </div></td>
       </tr>
      })

    return (
      <div>
        <div className="page-header">
          <h3 className="page-title"> Departments </h3>
          <nav aria-label="breadcrumb">
            <ol className="breadcrumb">
              <li className="breadcrumb-item"><a href="!#" onClick={event => event.preventDefault()}></a></li>
              <li> <Link to="/department/DepartmentAdd"> <button type="button" className='btn btn-primary'>Add Department</button></Link></li>
              <li className="breadcrumb-item active" aria-current="page"></li>
            </ol>
          </nav>
        </div>
        <div className="row">
        
          <div className="col-lg-12 grid-margin stretch-card">
            <div className="card">
              <div className="card-body">
                <h4 className="card-title"></h4>
                
                <div className="table-responsive">
                  <table className="table table-striped">
                    <thead>
                      <tr align ="center">
                        <th> Name </th>
                        <th> Description </th>       
                        <th> Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                   {updateTableList}
                          
                     
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          
        </div>
      </div>
    );
  }


export default DepartmentList
